// To do: arithmetic operations
/**
 * Adds two numbers together
 * @param {number} a first number
 * @param {number} b second number
 * @returns {number} sum
 */
const add = (a, b) => a + b;

const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};

/**
 * Divides dividend by divisor
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number} quotient
 * @throws {Error}
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed");
    const fraction = dividend / divisor;
    return fraction;
};

export default { add, subtract, divide, multiply }