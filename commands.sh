#!/bin/bash
mkdir -p ~/projects/stam/L03/unit-test #opettajan malli, ei minun käyttämä lokaatio
code ~/projects/stam/L03/unit-test #avaa vscoden annetusta sijainnista

git init #alustetaan projekti git-repositoryksi
npm init -y #luo pohjan package.js:lle, -y vastaa yes kaikkiin asennuksen aikaisiin kysymyksiin

npm install --save express #asennetaan express paketti
npm install --save-dev mocha chai #asennetaan mocha-testikirjasto ja chai-lisäri

echo "node_modules" > .gitignore #luo tiedoston ja lisää sinne rivin ""-merkkien sisältä

# git remote add origin https://gitlab.com/{username}/unit-test #luo yhteyden gitlabiin
# git remote set-url origin ...
# git push -u origin main

git add . --dry-run # näyttää mitä tiedostoja lisätään staging arealle

touch README.md #luo README.md nimisen tiedoston
mkdir src test #luo src test -kansion
touch src/main.js src/calc.js #samalla komentorivillä voi luoda useamman tiedoston
mkdir public
touch public/index.html #luo tiedoston kansioon
touch test/calc.test.js